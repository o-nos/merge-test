import models.A;
import models.B;

/**
 * Created by snos on 18.01.17.
 */
public class Main {

    public static void main(String[] args) {
        System.out.println("Hello world!");

        A a = new A();
        System.out.println(a);

        B b = new B();
        System.out.println(b);

        A a1 = new B();
        System.out.println(a1);

        A a2 = new B();
        System.out.println(a1);

        A a3 = new B();
        System.out.println(a1);
    }

}
